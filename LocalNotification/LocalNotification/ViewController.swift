//
//  ViewController.swift
//  LocalNotification
//
//  Created by kevin on 10/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var InputTextField: UITextField!
    @IBOutlet weak var notificacionLabel: UILabel!
    
    var notificationMessage = "El texto es: "
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. acceder al valor por medio del Key
        notificacionLabel.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]){
             (granted, error) in
            
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func GuardarButton(_ sender: Any) {
        //notificacionLabel.text = InputTextField.text
        
        //1.instanciar UserDefault
        let defaults = UserDefaults.standard
        
        //2. Guardar variables en default
        // int, Bool, Float, Double, String, Object[]
        
        defaults.set(InputTextField.text, forKey: "label")
        sendNotification()
    }
    
    func sendNotification(){
        notificationMessage += notificacionLabel.text!
        
        //1. Authorization request (esta en Didload)
        //2. Crear contenido de la notificacion
        let content = UNMutableNotificationContent()
        content.title = "Notification Title"
        content.subtitle = "Notification Subtitle"
        content.body = notificationMessage
        
        //2.1 crear acciones
        let repeatAction = UNNotificationAction(identifier: "repeat", title: "Repetir", options: [])
        let changeMessageAction = UNTextInputNotificationAction(identifier: "change", title: "Cambiar mensaje", options: [])
        
        //2.2 agregar acciones a un UNNotificationCategory
        let category = UNNotificationCategory(identifier: "actionsCat", actions: [repeatAction,changeMessageAction],
                                              intentIdentifiers: [], options: [])
        
        //2.3 agregar el categoryIdentifier al content
        
        content.categoryIdentifier = "actionsCat"
        
        //2.4 agregar categoria al UNUserNotificationCenter
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        //3.
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //4. Definir un identier para la notification
        let identifier = "Notification"
        
        //5. Crear un Request
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content,
                                                        trigger: trigger)
        
        //6. Añadir el request al UNUserNotificationCenter
        UNUserNotificationCenter.current().add(notificationRequest){ (error) in
            
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier{
        case "repeat":
            self.sendNotification()
            completionHandler()
            break
        case "change":
            let  txtResponse = response as! UNTextInputNotificationResponse
            notificationMessage += txtResponse.userText
            self.sendNotification()
            completionHandler()
        default:
            break
        }
        
    }

}

